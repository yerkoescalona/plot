#!/usr/bin/env python3

# Yerko Escalona

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.ticker import MultipleLocator

def errorbar(filenames, colors, linestyles, legends, hlines, style,
             x_minor_locator, x_major_locator, y_minor_locator, y_major_locator, 
             xlabel, ylabel, xmin, xmax, ymin, ymax, 
             x=0, y=1, yerr=2, outname="plot", **kwargs):

    if style:
        try:
            plt.style.use(style)
        except Exception as e:
            print("[!] WARNING: " + style + " does not exist\n Check styles available: " + " ".join(plt.style.available))


    if not colors:
        xs = np.arange(10)
        ys = [i + xs + (i * xs) ** 2 for i in range(len(filenames))]
        # args.colors = cm.viridis_r(np.linspace(0, 1, len(ys)))
        colors = cm.rainbow_r(np.linspace(0, 1, len(ys)))

    if not legends:
        legends = [f.replace('_', '\_') for f in filenames]

    if not linestyles:
        linestyles = ["solid" for f in range(len(filenames))]


    if len(filenames) != len(colors) or len(filenames) != len(linestyles) or len(filenames) != len(legends):
        print("[!] WARNING: length size of lists filenames, linestyles and legends differs")

    zval = 100
    opened_filenames = []
    for filename, color, linestyle, legend in zip(filenames, colors, linestyles, legends):
        print("[+] Opening file: ", filename)
        # open file
        try:
            data = np.loadtxt(open(filename, "rb"), comments=["@", "#"])
        except Exception as e:
            print("[!] WARNING: file %s not found \n error %s" % (filename, e))
            continue

        if np.size(data) == 0:
            print("[!] WARNING: file " + filename + " is empty")
            continue

        opened_filenames.append(filename)

        # get args.data
        x = data[:,x]
        y = data[:,y]
        yerr = data[:,yerr]

        zval -= 1

        plt.errorbar(x, y, yerr=yerr, color=color, linestyle=linestyle, label=legend, zorder=zval)


    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)

    if hlines:
        for hline in hlines:
            plt.axhline(y=hline, color="black", linestyle="dashed")

    plt.legend(loc="best")

    ax = plt.gca()
    if x_minor_locator: ax.xaxis.set_minor_locator(MultipleLocator(x_minor_locator))
    if x_major_locator: ax.xaxis.set_major_locator(MultipleLocator(x_major_locator))
    if y_minor_locator: ax.yaxis.set_minor_locator(MultipleLocator(y_minor_locator))
    if y_major_locator: ax.yaxis.set_major_locator(MultipleLocator(y_major_locator))

    plt.savefig(outname + ".png")
    plt.savefig(outname + ".svg")

    plt.show()

