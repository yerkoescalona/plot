from setuptools import setup

setup(
    name='plot',
    version='1.0.0',
    scripts=['bin/plot'],
    packages=['plot'],
    license='GPLv3.0',
    author='Yerko Escalona',
    author_email='yerko.escalona@protonmail.com',
    description='simple matplotlib wrapper',
    include_package_data=True,
)
